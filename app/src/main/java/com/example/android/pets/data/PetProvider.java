package com.example.android.pets.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import static com.example.android.pets.data.PetContract.*;
import static com.example.android.pets.data.PetContract.PetEntry.*;

/**
 * Created by roberto on 22/02/17.
 */

public class PetProvider extends ContentProvider {

    private static final String LOG_TAG = PetProvider.class.getSimpleName();

    /**
     * Constant for full access to table PETS
     */
    private static final int PETS = 100;

    /**
     * Constant for access to table PETS by ID.
     */
    private static final int PET_ID = 101;

    /**
     * Static Instance of UriMatcher to match given URL.
     */
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {

        //Matcher of contents://com.example.android.pets/pets
        sUriMatcher.addURI(PetContract.CONTENT_AUTHORITY, PetContract.PATH_PETS, PETS);

        //Matcher of contents://com.example.android.pets/pets/#
        sUriMatcher.addURI(PetContract.CONTENT_AUTHORITY, PetContract.PATH_PETS + "/#", PET_ID);

    }


    //Instance of PetDbHelper
    private PetDbHelper mDbHelper = null;

    @Override
    public boolean onCreate() {
        mDbHelper = new PetDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Cursor cursor = null;

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        final int action = sUriMatcher.match(uri);

        switch (action) {
            case PETS:
                cursor = db.query(PetContract.PetEntry.TABLE_NAME, projection, null, null, null, null, sortOrder);
                break;
            case PET_ID:
                selection = PetContract.PetEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = db.query(PetContract.PetEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("No match found for URI " + uri.toString());
        }

        //Notifica a la URI en el cursor para que actualice el UI
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }




    @Nullable
    @Override
    public String getType(Uri uri) {

        int match = sUriMatcher.match(uri);

        switch (match){
            case PETS:
                return CONTENT_LIST_TYPE;
            case PET_ID:
                return CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException("Unknown uri " + uri + "with match "+match);
        }

    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {

        final int match = sUriMatcher.match(uri);
        switch (match){
            case PETS:
                return insertPet(uri, values);
            default:
                throw new IllegalArgumentException("Insertion is not supported for "+ uri.toString());
        }
    }


    private Uri insertPet(Uri uri, ContentValues values){

        validateSanityCheckForInsert(values);

        //Open database for writing
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        //Insert pet into database
        long id = db.insert(PetContract.PetEntry.TABLE_NAME, null, values);

        if(id ==  -1){
            Log.v(LOG_TAG, "No se creó el registro para la mascota" );
            return null;
        }

        //Notifies to listeners when URI has changed
        getContext().getContentResolver().notifyChange(uri, null);

        //Appends the ID of new row to URI.
        return ContentUris.withAppendedId(uri, id);

    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Get writeable database
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        int deletedRows = 0;

        final int match = sUriMatcher.match(uri);

        switch (match) {
            case PETS:
                // Delete all rows that match the selection and selection args
                deletedRows = database.delete(PetEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case PET_ID:
                // Delete a single row given by the ID in the URI
                selection = PetEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                deletedRows = database.delete(PetEntry.TABLE_NAME, selection, selectionArgs);

                return deletedRows;
            default:
                throw new IllegalArgumentException("Deletion is not supported for " + uri);
        }

        if(deletedRows > 0) {
            //Notifies to listeners that URI has changed.
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return deletedRows;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection,
                      String[] selectionArgs) {
        int updatedRows = 0;
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case PETS:
                updatedRows = updatePet(uri, contentValues, selection, selectionArgs);
                break;
            case PET_ID:
                // For the PET_ID code, extract out the ID from the URI,
                // so we know which row to update. Selection will be "_id=?" and selection
                // arguments will be a String array containing the actual ID.
                selection = PetEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                updatedRows = updatePet(uri, contentValues, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Update is not supported for " + uri);
        }

        return updatedRows;
    }

    private boolean validateSanityCheckForInsert(ContentValues values){

        String name = values.getAsString(COLUMN_PET_NAME);

        if(name == null || name.trim().length() == 0){
            throw new IllegalArgumentException("Pet requires a name");
        }


        Integer gender = values.getAsInteger(PetContract.PetEntry.COLUMN_PET_GENDER);
        if(gender == null){
            throw new IllegalArgumentException("Pet requires a gender");
        }
        else if(!isValidGender(gender)){
            throw new IllegalArgumentException("Pet's gender should be 0, 1 or 2");
        }

        Integer weight = values.getAsInteger(PetContract.PetEntry.COLUMN_PET_WEIGHT);
        if(weight == null){
            throw new IllegalArgumentException("Pet requires a weight");
        }
        else if (weight < 0){
            throw new IllegalArgumentException("Pet's weight should be greater than or equal to 0");
        }

        return true;
    }


    private boolean validateSanityCheckForUpdate(ContentValues values){

        //Valida si existe el nombre en los valors.
        if(values.containsKey(COLUMN_PET_NAME)){
            String name = values.getAsString(COLUMN_PET_NAME);
            //Si existe, valida que no esté vacío.
            if(name != null && name.trim().length() == 0){
                throw new IllegalArgumentException("Pet's name should not be empty");
            }
        }

        //Valida si existe el género en los valores.
        if(values.containsKey(COLUMN_PET_GENDER)) {
            Integer gender = values.getAsInteger(COLUMN_PET_GENDER);
            //Si existe, valida que tenga un valor correcto.
            if (gender != null && !isValidGender(gender)) {
                throw new IllegalArgumentException("Pet's gender should be 0, 1 or 2");
            }
        }

        //Valida si existe el peso en los valores
        if(values.containsKey(COLUMN_PET_WEIGHT)) {
            Integer weight = values.getAsInteger(PetContract.PetEntry.COLUMN_PET_WEIGHT);
            //Si existe, valida que su valor sea >= 0.
            if (weight != null && weight < 0) {
                throw new IllegalArgumentException("Pet's weight should be greater than or equal to 0");
            }
        }

        return true;
    }

    private boolean isValidGender(int gender){
        if(gender == PetContract.PetEntry.GENDER_FEMALE || gender == PetContract.PetEntry.GENDER_MALE || gender == PetContract.PetEntry.GENDER_UNKNOWN){
            return true;
        }
        return false;
    }


    /**
     * Update pets in the database with the given content values. Apply the changes to the rows
     * specified in the selection and selection arguments (which could be 0 or 1 or more pets).
     * Return the number of rows that were successfully updated.
     */
    private int updatePet(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        if (values.size() == 0) {
            return 0;
        }

        validateSanityCheckForUpdate(values);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        int affectedRows = db.update(PetEntry.TABLE_NAME, values, selection, selectionArgs);

        if(affectedRows > 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return affectedRows;
    }
}

package com.example.android.pets.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by roberto on 21/02/17.
 */

public class PetDbHelper extends SQLiteOpenHelper {


    private static final String DB_NAME = "pets.db";
    private static final int DB_VERSION = 1;

    public PetDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        final String createTableStatement = new StringBuilder("CREATE TABLE ")
                .append(PetContract.PetEntry.TABLE_NAME)
                .append(" ( " )
                .append(PetContract.PetEntry._ID).append(' ').append("INTEGER PRIMARY KEY AUTOINCREMENT, ")
                .append(PetContract.PetEntry.COLUMN_PET_NAME).append(' ').append("TEXT NOT NULL, ")
                .append(PetContract.PetEntry.COLUMN_PET_BREED).append(' ').append("TEXT NOT NULL, ")
                .append(PetContract.PetEntry.COLUMN_PET_GENDER).append(' ').append("INTEGER NOT NULL, ")
                .append(PetContract.PetEntry.COLUMN_PET_WEIGHT).append(' ').append(" INTEGER NOT NULL DEFAULT 0 ")
                .append(" )")
                .toString();

        sqLiteDatabase.execSQL(createTableStatement);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}

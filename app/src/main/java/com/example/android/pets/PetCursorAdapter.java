package com.example.android.pets;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import static com.example.android.pets.data.PetContract.PetEntry.*;

/**
 * Created by roberto on 22/02/17.
 */

public class PetCursorAdapter extends CursorAdapter {

    public PetCursorAdapter(Context context, Cursor cursor){
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        //Creates the new view "list_item"
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);

    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        //Find fields to populate
        TextView nameTextView = (TextView) view.findViewById(R.id.name);
        TextView breedTextView = (TextView) view.findViewById(R.id.summary);

        //Get column indexes
        int idxName = cursor.getColumnIndex(COLUMN_PET_NAME);
        int idxBreed = cursor.getColumnIndex(COLUMN_PET_BREED);



        //Get values from cursor.
        String name = cursor.getString(idxName);
        String breed = cursor.getString(idxBreed);

        breed = TextUtils.isEmpty(breed)? context.getString(R.string.breed_unknown): breed;

        //Populate text views
        nameTextView.setText(name);
        breedTextView.setText(breed);
    }
}
